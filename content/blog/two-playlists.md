---
title:  Two playlists live inside you
date: 2020-04-18
image: "/deck.jpg"
summary: "Maybe the only _true_ two types of people are the ones who are tempted to categorize people into two distinct types and those who aren't. Clearly, I belong in the former camp. I digress."
tags: ["personal", "music"]
categories: ["personal"]
---

A lot of people have told me in no uncertain terms that "there are two types of people." The types in question are always different, but their binary is constant. Maybe the only _true_ two types of people are the ones who are tempted to categorize people into two distinct types and those who aren't. Clearly, I belong in the former camp. I digress.

It'll probably betray my macabre sensibilities, but there's one particular there-are-two-types-of-people that I often think about. It goes like:

"When it comes to falling from the top of a 30-story building, there are only two types of people. There's the type of person who looks up at the sky above them, and there's the type of person who looks down at the ground below."

Maybe this has stuck with me because of its undeniable internal logic. If you were actually falling from the top of a building, after all, you'd have to face one direction or another. Up or down. Receding sky or approaching ground. Over the years, I've idly wondered which type of person I'd be.

I've been thinking about this particular there-are-two-types-of-people again over the past few days, as I've been making a  playlist for myself. I make a lot of playlists, actually, for all sorts of occasions. I'll often make one just to note the passage of time. While my playlist curation is usually at least in part about pushing myself out of my comfort zone, this time, in the midst of the world's current flavor of chaos, I wanted to make a playlist specifically _about_ comfort. I gave it the working title "Chicken Soup". (I've since renamed it 😇😇😇.) I wanted to fill it with songs about joy and hope and love. I wanted to make a toolkit for myself—something to turn to as things inevitably Get Worse. I wanted a light I could turn on in the dark.

{{< angel-playlist >}}

Something weird happened every time I tried listen to it, though. The promised happiness rang hollow. Enjoying 😇😇😇 felt like willful denial. For every song I listened to, I heard the faint echo of another song about naked sorrow and desperation and grief, made more powerful by its omission. When I listened to Jason Isbell's insistence on a certain _her_ being _enough_ in "Cover Me Up", I couldn't help but think of "Elephant" when he grimly recalls, "Surrounded by her family, she saw she was dying alone." (Although maybe that speaks more to my encyclopedic knowledge of Jason Isbell's catalogue than anything else.)

I suddenly needed to put specific words to all the worst, most horrifying scenarios I could imagine unfolding. I needed to draw a picture of the monster in my mind so I could look it square in the eye. I needed to jump from a 30-story building and train my eyes toward the ground as it rushed toward me, unrelenting. So I made another playlist, of course. What else would I do? I called this one ☠️☠️☠️.

{{< skull-playlist >}}

I felt a certain, steady rhythm in the call-and-response I'd created. I pictured the two playlists as [Two-Face's coin](https://batman.fandom.com/wiki/Two-Face%27s_Coin)—one side shiny and clean, the other burnt black beyond recognition. Together they came together to form a complete entity. A single chaotic worldview.

I'd listen to the two playlists back-to-back as I went about my day. Making a sandwich as Townes Van Zandt waited around to die, scrubbing my toilet as Kacey Musgraves insisted there's a rainbow hangin' over my head. _I'm falling from a 30-story building right now_, I thought. Maybe I didn't have to choose to either look at the sky or at the ground. Maybe I could continuously spin around, taking in all 360 degrees. Maybe the resulting disorientation would somehow cushion the inevitable blow.

But then, after a while, ☠️☠️☠️ became exhausting. I started skipping songs that I knew had the potential to pack a certain emotional wallop. I couldn't keep taking in the same sorrow, over and over. Realities that once felt irresponsible to ignore began to feel useless to dwell on. I knew 35-year-old Carissa would have a freak accident. I knew Virtute the cat would run away. I knew Mount Eerie's wife would die. I knew he'd get on the boat and go to the place where they would have built a house, if she had lived. "You died, though. So I came here alone, with our baby and the dust of your bones."

So I listened to 😇😇😇 in isolation again, except it wasn't quite how I left it. Some of its songs still felt saccharine and superficial, at times. But some gained new meaning. Acknowledgement of darkness stuck out to me in ways I hadn't noticed before. In fact, the songs whose hope felt the most unimpeachable were the ones that teetered on the edge of despair.

If you read the the lyrics to Elvis Costello's "What's So Funny 'Bout Peace, Love, and Understanding?" in isolation, it probably seems like a better candidate for ☠️☠️☠️:

"And as I walked on through troubled times, my spirit gets so downhearted sometimes. So where are the strong? And who are the trusted? And where is the harmony?"

Most of the song is spent lamenting the state of the world. But there's such tenacity, such resilience, in the titular question that stubbornly repeats itself in each chorus, which dreams of a better future so distant that its ridiculousness is openly acknowledged. "What's so funny about peace, love, and understanding?"

Maybe there are two types of people after all. We're each both of them, depending on the day. On bad days, we only have the bandwidth to passively observe human misery—to cry alongside our fellow sufferers without attempting consolation. But on good days, we see that misery, but we also see a choice. A choice to acknowledge reality while working towards something better, no matter how vague or fleeting, regardless of if we'll experience the fruits of that unnamable Better-ness in our own lifetimes. A choice to look up at the sky.
